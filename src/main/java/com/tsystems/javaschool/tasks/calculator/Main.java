package com.tsystems.javaschool.tasks.calculator;

public class Main {
    public static void main(String[] args) {
        Calculator c = new Calculator();

        System.out.println(c.evaluate("10/(2-7+3)*4"));
    }
}
