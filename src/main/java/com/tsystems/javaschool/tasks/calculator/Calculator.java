package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        //Step 0. Validate input
        try {
            validate(statement);
        } catch (CannotBeEvaluatedException e) {
            return null;
        }

        ArrayList<String> splitted = new ArrayList<>();
        //Step 1. Get rid of parentheses
        try {
            findPar(statement, splitted);
        } catch (CannotBeEvaluatedException e) {
            return null;
        }
        //Step 2. Split by +/-/*//
        try {
            split(splitted);
        } catch (CannotBeEvaluatedException e) {
            return null;
        }

        //Step 3. Simplify (apply * and / )
        try {
            while (splitted.contains("*") || splitted.contains("/"))
                simplyfy(splitted);
        } catch (CannotBeEvaluatedException e) {
            return null;
        }
        //Step 4. calculate from prepared array, which consist of digits and signs
        double res = 0;
        while (splitted.size() != 1)
            res = calcNext(splitted);
        res = Double.parseDouble(splitted.get(0));
        res = round(res, 4);
        DecimalFormat format = new DecimalFormat("0.####");
        return format.format(res).replace(',','.');
    }

    public void validate(String str) {
        //Pattern p = Pattern.compile("0-9\\*/().\\+\\-");
//        Pattern p = Pattern.compile("[0-9].");
//        Matcher m = p.matcher(str);
//        if (m.find())
//            throw new CannotBeEvaluatedException();
//        if (Pattern.matches("0-9*/().\\+\\-", str) == true)
//            throw new CannotBeEvaluatedException();
        if (str == null || str.length() == 0)
            throw new CannotBeEvaluatedException();
        boolean isDot = false;
        boolean isSign = false;
        boolean parOpened = false;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '.') {
                if (isDot == true)
                    throw new CannotBeEvaluatedException();
                isDot = true;
            }
            else if (str.charAt(i) == '+' || str.charAt(i) == '-'
            || str.charAt(i) == '*' || str.charAt(i) == '/') {
                if (isSign == true)
                    throw new CannotBeEvaluatedException();
                isSign = true;
            }
            else if (str.charAt(i) == '(') {
                parOpened = true;
            }
            else if (str.charAt(i) == ')') {
                if (parOpened == false)
                    throw new CannotBeEvaluatedException();
                parOpened = false;
            }
            else if (Character.isDigit(str.charAt(i)))
            {
                isDot = isSign = false;
            }
            else
                throw new CannotBeEvaluatedException();
        }
        if (parOpened == true || isSign == true)
            throw new CannotBeEvaluatedException();
    }

    public void simplyfy(ArrayList<String> splitted) {
        int index1 = splitted.indexOf("*");
        int index2 = splitted.indexOf("/");
        int index = 0;
        if (index1 == -1)
            index = index2;
        else if (index2 == -1)
            index = index1;
        else
            index = Math.min(index1, index2);
        double res = 0;
        double resLeft = Double.parseDouble(splitted.get(index - 1));
        double resRight = Double.parseDouble(splitted.get(index + 1));
        if ("*".equals(splitted.get(index))) {
            res = resLeft * resRight;
        } else {
            if (resRight == 0)
                throw new CannotBeEvaluatedException();
            res = resLeft / resRight;
        }
        splitted.remove(index - 1);
        splitted.remove(index - 1);
        splitted.set(index - 1, String.valueOf(res));
    }

    public void split(ArrayList<String> splitted) {
        int privPos;
        int initSize = splitted.size();
        for (int i = 0; i < initSize; ) {
            privPos = 0;
            for (int index = 0; index < splitted.get(i).length(); index++) {
                if (splitted.get(i).charAt(index) == '+'
                        || (splitted.get(i).charAt(index) == '-' && index != 0)
                        || splitted.get(i).charAt(index) == '*'
                        || splitted.get(i).charAt(index) == '/') {
                    if (index != privPos)
                        splitted.add(splitted.get(i).substring(privPos, index));
                    splitted.add(splitted.get(i).substring(index, index + 1));
                    index = privPos = index + 1;
                }
            }
            if (privPos != splitted.get(i).length())
                splitted.add(splitted.get(i).substring(privPos));
            splitted.remove(i);
            initSize--;
        }
    }

    public double calcNext(ArrayList<String> splitted) {
        double tmp = 0;
        int i = 0;
            if ("-".equals(splitted.get(i + 1)))
                tmp = Double.parseDouble(splitted.get(i++)) - Double.parseDouble(splitted.get(++i));
            else if ("+".equals(splitted.get(i + 1)))
                tmp = Double.parseDouble(splitted.get(i++)) + Double.parseDouble(splitted.get(++i));
        splitted.remove(0);
        splitted.remove(0);
        splitted.set(0, String.valueOf(tmp));
        return tmp;
    }

    public static double round(double value, int places) {
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public void findPar(String statement, ArrayList<String> splitted) {
        int pos = 0, curr_pos = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                if (i != 0) {
                    curr_pos = i;
                    splitted.add(statement.substring(pos, curr_pos));
                }
                i = pos = curr_pos + 1;
                while (i < statement.length()) {
                    if (statement.charAt(i) == '(')
                        throw new CannotBeEvaluatedException();
                    else if (statement.charAt(i) == ')') {
                        curr_pos = i;
                        String tmp_str = statement.substring(pos, curr_pos);
                        ArrayList<String> tmp_list = new ArrayList<>();
                        findPar(tmp_str, tmp_list); //recursion
                        split(tmp_list);
                        while (tmp_list.contains("*") || tmp_list.contains("/"))
                            simplyfy(tmp_list);
                        double res;
                        while (tmp_list.size() != 1)
                            res = calcNext(tmp_list);
                        res = Double.parseDouble(tmp_list.get(0));
                        tmp_list.clear();
                        tmp_list.add(String.valueOf(res));
                        splitted.addAll(tmp_list);
                        pos = i + 1;
                        break;
                    }
                    i++;
                }
            }
        }
        if (pos != statement.length() && statement.charAt(pos) != ')' )
            splitted.add(statement.substring(pos));
    }
}
