package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        // check if null
        if (y == null || x == null)
            throw new IllegalArgumentException();
        // always true conditions
        if (x.size() == 0)
            return true;
        // always false conditions
        else if ((x.size() > y.size()) || y.size() == 0)
            return false;
        // return true only if y ends, but x didn't end
        int x_pos = 0, y_pos = 0;
        while (x_pos < x.size() && y_pos < y.size()) {
            if (x.get(x_pos).equals(y.get(y_pos))) {
                x_pos++;
            }
            y_pos++;
        }
        if (x_pos == x.size())
            return true;
        return false;
    }
}
