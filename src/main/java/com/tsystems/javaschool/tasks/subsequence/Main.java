package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Main {
    public static void main(String[] args) {
        Subsequence subsequence = new Subsequence();
        //given
        List x = Stream.of(1, 3, 5, 7, 9).collect(toList());
        List y = Stream.of(10, 1, 2, 3, 4, 5, 7, 9, 20).collect(toList());

        //run
        boolean result = subsequence.find(x, y);
        System.out.println(result);
    }
}
