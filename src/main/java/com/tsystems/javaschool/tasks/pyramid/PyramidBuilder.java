package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (Throwable t) {
            throw new CannotBuildPyramidException();
        }

        //check if list size corresponds to a triangle number
        int size = inputNumbers.size();
        double fullSquare = Math.sqrt(8 * size + 1);
        if (fullSquare % 1 != 0)
            throw new CannotBuildPyramidException();

        //find pyramid height and width
        int hei = ((int) fullSquare - 1) / 2;
        int wid = hei + (hei - 1);

        //fill an int array
        int[][] arr = new int[hei][wid];
        int margin = (wid - 1) / 2;
        int cutSize = 1;
        int pos = 0;
        int wPos;
        for (int i = 0; i < hei; i++) {
            wPos = 0;
            for (int j = 0; j < margin; j++) {
                arr[i][wPos++] = 0;
            }
            for (int j = 0; j < cutSize; j++) {
                arr[i][wPos++] = inputNumbers.get(pos++);
                if (j + 1 < cutSize) {
                    arr[i][wPos++] = 0;
                }
            }
            for (int j = 0; j < margin; j++) {
                arr[i][wPos++] = 0;
            }
            margin--;
            cutSize++;
        }
        return arr;
    }
}
