package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PyramidBuilder pyramidBuilder = new PyramidBuilder();

        List<Integer> input = Arrays.asList(1, 3, 2, 9, 4, 5);

        pyramidBuilder.buildPyramid(input);
    }
}
